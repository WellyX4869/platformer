﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class PauseHandler : MMSingleton<PauseHandler>
    {
        [Header("Pause UI")]
        [SerializeField] Image continueImage;
        [SerializeField] Sprite continueDefaultSprite;
        [SerializeField] Sprite continueActiveSprite;
        [SerializeField] Image optionsImage;
        [SerializeField] Sprite optionsDefaultSprite;
        [SerializeField] Sprite optionsActiveSprite;
        [SerializeField] Image mainMenuImage;
        [SerializeField] Sprite mainMenuDefaultSprite;
        [SerializeField] Sprite mainMenuActiveSprite;

        protected override void Awake()
        {
            base.Awake();
        }

        private void Start()
        {
            ResetAll();
        }

        public void ResetAll()
        {
            continueImage.sprite = continueDefaultSprite;
            optionsImage.sprite = optionsDefaultSprite;
            mainMenuImage.sprite = mainMenuDefaultSprite;
        }

        #region HOVER/UNHOVER
        public void HoverContinueButton()
        {
            continueImage.sprite = continueActiveSprite;
        }

        public void UnhoverContinueButton()
        {
            continueImage.sprite = continueDefaultSprite;
        }

        public void HoverOptionsButton()
        {
            optionsImage.sprite = optionsActiveSprite;
        }

        public void UnhoverOptionsButton()
        {
            optionsImage.sprite = optionsDefaultSprite;
        }

        public void HoverMainMenuButton()
        {
            mainMenuImage.sprite = mainMenuActiveSprite;
        }

        public void UnhoverMainMenuButton()
        {
            mainMenuImage.sprite = mainMenuDefaultSprite;
        }
        #endregion
    }
}
