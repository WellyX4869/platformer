﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MoreMountains.CorgiEngine
{
    public class CustomGameManager : MMPersistentSingleton<CustomGameManager>
    {
        public static CustomGameManager Instance;

        [Header("Pause Splash")]
        [SerializeField] GameObject pauseSplash;

        [Header("Level Clear Splash")]
        [SerializeField] GameObject levelClearSplash;
        [SerializeField] Text scoreText;
        [SerializeField] Text levelClearedTimeText;
        bool levelCleared = false;
        float levelClearedTime = 0f;
        int score = 0;

        [Header("Game Over Splash")]
        [SerializeField] GameObject gameOverSplash;

        [Header("Confirm Quit Splash")]
        [SerializeField] GameObject confirmQuitSplash;

        protected virtual void Awake()
        {
            if(Instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
            }

            Time.timeScale = 1f;
        }

        void Start()
        {
            Reset();
        }

        void Reset()
        {
            pauseSplash.SetActive(false);
            levelClearSplash.SetActive(false);
            gameOverSplash.SetActive(false);
            confirmQuitSplash.SetActive(false);

            levelCleared = false;
            levelClearedTime = 0f;
            score = 0;
        }

        void Update()
        {
            if(!levelCleared && !GameManager.Instance.Paused)
                levelClearedTime += Time.deltaTime;
        }

        #region LEVEL CLEARED
        public void ShowLevelCleared()
        {
            SetupLevelCleared();
            Time.timeScale = 0f;
            levelClearSplash.SetActive(true);
        }

        public void SetupLevelCleared()
        {
            levelCleared = true;
            score = GameManager.Instance.CurrentLives * 1000;
            scoreText.text = score.ToString();

            float hours = Mathf.Floor(levelClearedTime / 3600);            
            float minutesTimer = Mathf.Floor(levelClearedTime % 3600);
            float minutes = Mathf.Floor(minutesTimer / 60);
            float seconds = Mathf.Floor(minutesTimer % 60);
            levelClearedTimeText.text = hours.ToString("00") + "." + minutes.ToString("00") + "." + seconds.ToString("00");
        }
        #endregion

        #region GAME OVER
        public void ShowGameOver()
        {
            if (Time.timeScale > 0.0f)
            {
                Time.timeScale = 0f;
            }
            gameOverSplash.SetActive(true);
        }
        #endregion

        public void RestartLevel()
        {
            GameManager.Instance.ResetLives();
            if (GameManager.Instance.Paused) { CorgiEngineEvent.Trigger(CorgiEngineEventTypes.TogglePause); }
            SceneManager.LoadScene(0);
            Reset();
        }

        #region CONFIRM QUIT
        public void ShowConfirmQuit()
        {
            confirmQuitSplash.SetActive(true);
        }

        public void CancelQuit()
        {
            confirmQuitSplash.SetActive(false);
        }

        public void ConfirmQuit()
        {
            Application.Quit();
        }
        #endregion

        #region OPTIONS
        // public void ShowOptions() 
        // {
        //     optionsSplash.SetActive(true);
        // }

        // public void CloseOptions()
        // {
        //     optionsSplash.SetActive(false);
        // }

        // public void SetupOptions()
        // {
        //     var soundManager = MMSoundManager.Instance;
        //     musicSlider.value = soundManager.settingsSo.GetTrackVolume(MMSoundManager.MMSoundManagerTracks.Music);
        //     sfxSlider.value = soundManager.settingsSo.GetTrackVolume(MMSoundManager.MMSoundManagerTracks.Sfx);

        //     for(int i = 0; i < resolutions.Count; i++)
        //     {
        //         if(resolutions[i].x == Screen.currentResolution.width && resolutions[i].y == Screen.currentResolution.height)
        //         {
        //             resolutionIndex = i;
        //         }
        //     }
        //     int width = resolutions[resolutionIndex].x;
        //     int height = resolutions[resolutionIndex].y;
        //     resolutionText.text = width.ToString() + "x" + height.ToString();
        // }

        // public void ChangeMusicVolume(float value)
        // {
        //     var soundManager = MMSoundManager.Instance;
        //     soundManager.SetVolumeTrack(MMSoundManager.MMSoundManagerTracks.Music, value);
        // }

        // public void ChangeSFXVolume(float value)
        // {
        //     var soundManager = MMSoundManager.Instance;
        //     soundManager.SetVolumeTrack(MMSoundManager.MMSoundManagerTracks.Sfx, value);
        // }

        // public void ChoosePreviousResolution()
        // {
        //     if(resolutionIndex <= 0) { return; }
        //     resolutionIndex--;
        //     int width = resolutions[resolutionIndex].x;
        //     int height = resolutions[resolutionIndex].y;
        //     resolutionText.text = width.ToString() + "x" + height.ToString();
        //     SetResolution(resolutions[resolutionIndex]);
        // }

        // public void ChooseNextResolution()
        // {
        //     if(resolutionIndex >= resolutions.Count-1) { return; }
        //     resolutionIndex++;
        //     int width = resolutions[resolutionIndex].x;
        //     int height = resolutions[resolutionIndex].y;
        //     resolutionText.text = width.ToString() + "x" + height.ToString();
        //     SetResolution(resolutions[resolutionIndex]);
        // }

        // private void SetResolution(Vector2Int resolution)
        // {
        //     Screen.SetResolution(resolution.x, resolution.y, Screen.fullScreenMode);
        // }
        #endregion
    }
}