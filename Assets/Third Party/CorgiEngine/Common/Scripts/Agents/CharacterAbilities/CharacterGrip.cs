﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{	
	/// <summary>
	/// Add this component to a character and it'll be able to grip level elements that have the Grip component
	/// Animator parameters : Gripping (bool)
	/// </summary>
	[AddComponentMenu("Corgi Engine/Character/Abilities/Character Grip")] 
	public class CharacterGrip : CharacterAbility 
	{
		/// This method is only used to display a helpbox text at the beginning of the ability's inspector
		public override string HelpBoxText() { return "Add this component to a character and it'll be able to grip level elements that have the Grip component."; }
		/// The duration (in seconds) during which a character can't grip again after exiting a grip
		[Tooltip("The duration (in seconds) during which a character can't grip again after exiting a grip")]
		public float BufferDurationAfterGrip = 0.3f;
		/// Returns true if the character can grip right now, false otherwise
		public bool CanGrip { get { return (Time.time - _lastGripTimestamp > BufferDurationAfterGrip); }}

		protected CharacterJump _characterJump;
		protected float _lastGripTimestamp = 0f;
		protected Grip _gripTarget;
        protected bool _attached = false;

		// For Climbing Purposes
		[Header("CLIMBING")]
		public float detachOffsetX;
		public bool CanClimb;
		public float climbSpeed;
		float localYPos = 0;
		bool firstTimeAttached = false;
		Collider2D colliderGripTarget;
		[HideInInspector] public bool IsSwinged = false;

		// animation parameters
		protected const string _grippingAnimationParameterName = "Gripping";
        protected int _grippingAnimationParameter;

        /// <summary>
        /// On Start() we grab our character jump component
        /// </summary>
        protected override void Initialization()
		{
			base.Initialization();
			_characterJump = _character?.FindAbility<CharacterJump>();
		}

		/// <summary>
		/// Every frame we check to see if we should be gripping
		/// </summary>
		public override void ProcessAbility()
		{
			base.ProcessAbility();
			Grip();
			Detach ();
		}

		/// <summary>
		/// A public method to have the character grip the specified target
		/// </summary>
		/// <param name="gripTarget">Grip target.</param>
		public virtual void StartGripping(Grip gripTarget)
		{
			if (!CanGrip)
            {
                return;
            }

            PlayAbilityStartFeedbacks();
            _attached = true;
            _gripTarget = gripTarget;
			colliderGripTarget = gripTarget.collider;
			_movement.ChangeState (CharacterStates.MovementStates.Gripping);
		}

		/// <summary>
		/// Called at update, handles gripping to Grip components (ropes, etc)
		/// </summary>
		protected virtual void Grip()
		{
			// if we're gripping to something, we disable the gravity
			if (_movement.CurrentState == CharacterStates.MovementStates.Gripping)
			{	
				_controller.SetForce(Vector2.zero);
				_controller.GravityActive(false);		
				if (_characterJump != null)
				{
					_characterJump.ResetNumberOfJumps();
                }

				Vector3 controllerPos = _controller.transform.position;
				Vector3 centerGripPos = colliderGripTarget.bounds.center;

				// Detach Checking
				//string detachLog = "Min X = " + (colliderGripTarget.bounds.min.x - detachOffsetX) + "\n";
				//detachLog += "Player X = " + (controllerPos.x) + "\n";
				//detachLog += "Max X = " + (colliderGripTarget.bounds.max.x + detachOffsetX);
				//Debug.Log(detachLog);

				if (controllerPos.x  < colliderGripTarget.bounds.min.x - detachOffsetX || controllerPos.x > colliderGripTarget.bounds.max.x + detachOffsetX)
				{
					Detach();
                }

				// Climb Handler
				if (firstTimeAttached && (CanClimb || _verticalInput != 0))
				{
					if(colliderGripTarget != null && !IsSwinged)
                    {
						float minBoundsY = (colliderGripTarget.bounds.min.y);
						float maxBoundsY = (colliderGripTarget.bounds.max.y);
						float extentsY = (maxBoundsY - minBoundsY) / 2f;

						localYPos += _verticalInput * climbSpeed * 0.01f;
						localYPos = Mathf.Clamp(localYPos, -extentsY, extentsY);
					}
					_controller.SetTransformPosition(new Vector3(centerGripPos.x, centerGripPos.y + localYPos, centerGripPos.z));
				}
				else
                {
                    if (CanClimb)
                    {
						// ROTATED -90
						localYPos = controllerPos.y - centerGripPos.y;
						_controller.SetTransformPosition(new Vector3(centerGripPos.x, centerGripPos.y + localYPos, centerGripPos.z));
					}
					else 
					{ 
						_controller.SetTransformPosition(_gripTarget.transform.position + _gripTarget.GripOffset);
					}
					firstTimeAttached = true;
				}
			}
		}	

		/// <summary>
		/// Checks whether we should stop gripping or not
		/// </summary>
		protected virtual void Detach()
		{
			if ((_movement.CurrentState != CharacterStates.MovementStates.Gripping)
				&& (_movement.PreviousState == CharacterStates.MovementStates.Gripping)
				&& (_attached))
			{
				_lastGripTimestamp = Time.time;
				firstTimeAttached = false;
                _attached = false;
				IsSwinged = false;
				colliderGripTarget = null;
				localYPos = 0;
                StopStartFeedbacks();
                PlayAbilityStopFeedbacks();
            }
		}

		/// <summary>
		/// Adds required animator parameters to the animator parameters list if they exist
		/// </summary>
		protected override void InitializeAnimatorParameters()
		{
			RegisterAnimatorParameter (_grippingAnimationParameterName, AnimatorControllerParameterType.Bool, out _grippingAnimationParameter);
		}

		/// <summary>
		/// At the end of each cycle, we send our character's animator the current gripping status
		/// </summary>
		public override void UpdateAnimator()
		{
            MMAnimatorExtensions.UpdateAnimatorBool(_animator, _grippingAnimationParameter, (_movement.CurrentState == CharacterStates.MovementStates.Gripping), _character._animatorParameters, _character.PerformAnimatorSanityChecks);
		}

		/// <summary>
		/// On reset ability, we cancel all the changes made
		/// </summary>
		public override void ResetAbility()
		{
			base.ResetAbility();
			if (_condition.CurrentState == CharacterStates.CharacterConditions.Normal)
			{
				Detach();
			}
			MMAnimatorExtensions.UpdateAnimatorBool(_animator, _grippingAnimationParameter, false, _character._animatorParameters, _character.PerformAnimatorSanityChecks);
		}

		public float GetHorizontalInput()
        {
			return _horizontalInput;
        }

		public float GetVerticalInput()
        {
			return _verticalInput;
        }
		
		public bool IsStillAttachedToAnotherGrip()
        {
			return _attached;
        }

		public void ResetAttached()
        {
			firstTimeAttached = false;
			localYPos = _controller.transform.position.y - (_gripTarget.transform.position.y + _gripTarget.GripOffset.y);
		}
	}
}