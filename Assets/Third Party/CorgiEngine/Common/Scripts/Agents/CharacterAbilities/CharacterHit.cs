﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    /// <summary>
    /// This component will apply animation if the character is getting hit from enemy
    /// Animation parameter : FallDamage, bool, true the frame the character takes fall damage
    /// </summary>
    [AddComponentMenu("Corgi Engine/Character/Abilities/Character Hit")]
    public class CharacterHit : CharacterAbility
    {
        public override string HelpBoxText() { return "This component will apply animation if the character is getting hit from enemy."; }

        [HideInInspector] public bool _hitAnimationEnabled;
        [Tooltip("Hit Animation Length")]
        public AnimationClip _hitAnimationClip;
        [Tooltip("Hit Trigger Range is range of how many hits needed to trigger the hit animation")]
        public Vector2Int _hitTriggerRange;

        // animation parameters
        protected const string _hitAnimationParameterName = "Hit";
        protected int _hitAnimationParameter;

        float _hitAnimationClipLength = 0f;
        float _hitAnimationElapsedTime = 0f;
        int _hitCounter = 0;
        int _hitTreshold = -1;
        bool _isHit;

        /// <summary>
        /// Get Hit will be called every time the character got hit by enemy or anything
        /// </summary>
        public void GetHit()
        {
            _hitCounter++;
            _hitTreshold = _hitTreshold > 0 ? _hitTreshold : Random.Range(_hitTriggerRange.x, _hitTriggerRange.y+1);
            _hitAnimationEnabled = _hitCounter >= _hitTreshold ? true : false;
            if (_hitAnimationEnabled)
            {
                _hitCounter = 0;
                _hitTreshold = -1;
            }
        }

        /// <summary>
        /// On Update, we check if we're taking flight, and if we should take damage
        /// </summary>
        public override void ProcessAbility()
        {
            base.ProcessAbility();
            _isHit = false;

            if(CanTakeDamage())
                _isHit = _hitAnimationEnabled;
        }

        /// <summary>
        /// This method returns true if the character is in a state that can take damage.
        /// Don't hesitate to extend and override this method to specify your own rules
        /// </summary>
        /// <returns></returns>
        protected virtual bool CanTakeDamage()
        {
            return (_character.MovementState.CurrentState != CharacterStates.MovementStates.LadderClimbing
                && _character.MovementState.CurrentState != CharacterStates.MovementStates.SwimmingIdle
                && _character.MovementState.CurrentState != CharacterStates.MovementStates.Diving
                && _character.MovementState.CurrentState != CharacterStates.MovementStates.Flying
                && _character.MovementState.CurrentState != CharacterStates.MovementStates.Gliding
                && _character.MovementState.CurrentState != CharacterStates.MovementStates.WallClinging
                && _character.MovementState.CurrentState != CharacterStates.MovementStates.Jetpacking);
        }

        /// <summary>
		/// Adds required animator parameters to the animator parameters list if they exist
		/// </summary>
		protected override void InitializeAnimatorParameters()
        {
            RegisterAnimatorParameter(_hitAnimationParameterName, AnimatorControllerParameterType.Bool, out _hitAnimationParameter);
            if (_hitAnimationClip != null) { _hitAnimationClipLength = _hitAnimationClip.length; }
        }

        /// <summary>
        /// At the end of each cycle, we send our character's animator the current fall damage status
        /// </summary>
        public override void UpdateAnimator()
        {
            MMAnimatorExtensions.UpdateAnimatorBool(_animator, _hitAnimationParameter, _isHit, _character._animatorParameters, _character.PerformAnimatorSanityChecks);

            if (_hitAnimationEnabled)
            {
                _hitAnimationElapsedTime += Time.deltaTime;

                if(_hitAnimationElapsedTime >= _hitAnimationClipLength)
                {
                    _hitAnimationElapsedTime = 0f;   
                    _hitAnimationEnabled = false;
                }
            }
        }
    }
}
