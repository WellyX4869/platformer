﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    /// <summary>
    /// Add this component to an object with a 2D collider and it'll be grippable by any Character equipped with a CharacterGrip
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    [AddComponentMenu("Corgi Engine/Environment/Grip")]
    public class Grip : MonoBehaviour
    {
        [MMInformation("Add this component to an object with a Collider2D on it, and any character with a CharacterGrip ability will be able to hang on to it. You can also specify an offset for the character when gripping.", MoreMountains.Tools.MMInformationAttribute.InformationType.Info, false)]

        /// the offset to apply to the gripped character's position when gripping
        [Tooltip("the offset to apply to the gripped character's position when gripping")]
        public Vector3 GripOffset = Vector3.zero;

        protected MMPathMovement _mmPathMovement;

        // For Swing on Rope
        [Header("For Swing on Rope Purposes Only")]
        public bool CanBeSwinged = false;
        [SerializeField] float swingForce = 1f;
        bool attached = false;
        CharacterGrip playerGrip = null;
        Rigidbody2D rb;
        HingeJoint2D hj;
        public Collider2D collider;

        /// <summary>
        /// On Start we grab our MMPathMovement component
        /// </summary>
        protected virtual void Start()
        {
            _mmPathMovement = this.gameObject.GetComponent<MMPathMovement>();
            collider = this.gameObject.GetComponent<Collider2D>();

            if (CanBeSwinged)
            {
                rb = this.gameObject.GetComponent<Rigidbody2D>();
                hj = this.gameObject.GetComponent<HingeJoint2D>();
            }
        }

        protected virtual void Update()
        {
            if (!CanBeSwinged) return;

            if (attached)
            {
                // Horizontal Input
                float horizontalInput = playerGrip.GetHorizontalInput();
                if (horizontalInput != 0)
                {
                    playerGrip.IsSwinged = true;
                    if (horizontalInput < 0)
                        rb.AddRelativeForce(this.transform.right * swingForce * Time.deltaTime);
                    else
                        rb.AddRelativeForce(-this.transform.right * swingForce * Time.deltaTime);
                }
                else
                {
                    playerGrip.IsSwinged = false;
                }
            }
        }

        /// <summary>
        /// When an object collides with the grip, we check to see if it's a compatible character, and if yes, we change its state to Gripping
        /// </summary>
        /// <param name="collider">Collider.</param>
        protected virtual void OnTriggerEnter2D(Collider2D collider)
        {
            CharacterGrip characterGrip = collider.gameObject.MMGetComponentNoAlloc<Character>()?.FindAbility<CharacterGrip>();
            if (characterGrip == null)
            {
                if (CanBeSwinged) { playerGrip = null; attached = false; }
                return;
            }

            if (playerGrip != null && playerGrip.IsSwinged)
            {
                return;
            }

            if (CanBeSwinged && characterGrip.IsStillAttachedToAnotherGrip())
            {
                characterGrip.ResetAttached();
            }

            characterGrip.StartGripping(this);

            // For Swing on Rope Purposes
            if (CanBeSwinged)
            {
                playerGrip = characterGrip;
                attached = true;
            }

            if (_mmPathMovement != null)
            {
                _mmPathMovement.CanMove = true;
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D collider)
        {
            CharacterGrip characterGrip = collider.gameObject.MMGetComponentNoAlloc<Character>()?.FindAbility<CharacterGrip>();
            if (characterGrip != null)
            {
                if (CanBeSwinged) { playerGrip = null; attached = false; }
                return;
            }
        }
    }
}