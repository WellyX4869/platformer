using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class OptionsHandler : MonoBehaviour
{
    [Header("Menu")]
    [SerializeField] GameObject backgroundOptions;
    [SerializeField] GameObject optionsMenu;
    [SerializeField] GameObject audioMenu;
    [SerializeField] GameObject videoMenu;
    [SerializeField] GameObject controlsMenu;
   
    [Header("Audio Menu")]
    [SerializeField] float soundVolume;
    [SerializeField] float musicVolume;
    [SerializeField] float masterVolume;
    [SerializeField] Slider soundSlider;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider masterSlider;

    [Header("Video Menu")]
    [SerializeField] string resolutionString;
    [SerializeField] string fullScreenString;
    [SerializeField] float brightnessValue;
    [SerializeField] Slider brightnessSlider;

    [Header("Controls Menu")]
    [SerializeField] GameObject keyboardMenu;
    [SerializeField] GameObject controllerMenu;

    private void Start() 
    {
        // Close Audio
        audioMenu.SetActive(false);

        // Close Video
        videoMenu.SetActive(false);

        // Close Controls
        keyboardMenu.SetActive(false);
        controllerMenu.SetActive(false);
        controlsMenu.SetActive(false);

        // Close Options
        optionsMenu.SetActive(false);
        backgroundOptions.SetActive(false);
    }

    #region OPTIONS MENU
    public void ShowOptionsMenu()
    {
        CommonHandler.instance.FadeInAnimation(backgroundOptions);
        CommonHandler.instance.FadeInAnimation(optionsMenu);
    }

    public void CloseOptionsMenu()
    {
        CommonHandler.instance.FadeOutAnimation(optionsMenu);
    }
    public void CloseOptions()
    {
        CommonHandler.instance.FadeOutAnimation(optionsMenu);
        CommonHandler.instance.FadeOutAnimation(backgroundOptions);
    }
    #endregion

    #region AUDIO MENU
    public void ShowAudioMenu()
    {
        CloseOptionsMenu();
        CommonHandler.instance.FadeInAnimation(audioMenu);
    }

    public void ChangeSoundVolume(float value)
    {
        soundVolume = value;
        soundSlider.value = value;
    }
    public void ChangeMusicVolume(float value)
    {
        musicVolume = value;
        musicSlider.value = value;
    }
    public void ChangeMasterVolume(float value)
    {
        masterVolume = value;
        masterSlider.value = value;
    }

    public void ResetDefaultAudio()
    {
        ChangeSoundVolume(1f);
        ChangeMusicVolume(1f);
        ChangeMasterVolume(1f);
    }

    public void CloseAudioMenu()
    {
        CommonHandler.instance.FadeOutAnimation(audioMenu);
        optionsMenu.SetActive(true);
    }
    #endregion

    #region VIDEO MENU
    public void ShowVideoMenu()
    {
        CloseOptionsMenu();
        CommonHandler.instance.FadeInAnimation(videoMenu);
    }

    void ChangeResolutionValue(int value)
    {

    }

    void ChangeFullscreen()
    {

    }

    public void ChangeBrightnessValue(float value)
    {
        brightnessValue = value;
        brightnessSlider.value = value;
    }

    public void ResetDefaultVideo()
    {
        resolutionString = "1920 x 1080";

        ChangeBrightnessValue(1f);
    }

    public void CloseVideoMenu()
    {
        CommonHandler.instance.FadeOutAnimation(videoMenu);
        optionsMenu.SetActive(true);
    }
    #endregion

    #region  CONTROLS MENU
    public void ShowControlsMenu()
    {
        CloseOptionsMenu();
        CommonHandler.instance.FadeInAnimation(controlsMenu);
    }
  
    #region KEYBOARD MENU
    public void ShowKeyboardControls()
    {
        CommonHandler.instance.FadeInAnimation(keyboardMenu);
    }

    public void RestoreDefaultKeyboard()
    {

    }

    public void CloseKeyboardControls()
    {
        CommonHandler.instance.FadeOutAnimation(keyboardMenu);
    }
    #endregion
    #region CONTROLLER MENU
    public void ShowControllerControls()
    {
        CommonHandler.instance.FadeInAnimation(controllerMenu);
    }

    public void RestoreDefaultController()
    {

    }

    public void CloseControllerControls()
    {
        CommonHandler.instance.FadeOutAnimation(controllerMenu);
    }
    #endregion

    public void CloseControlsMenu()
    {
        CommonHandler.instance.FadeOutAnimation(controlsMenu);
        optionsMenu.SetActive(true);
    }
    #endregion
}