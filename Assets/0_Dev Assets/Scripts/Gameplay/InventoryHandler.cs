using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryHandler : MonoBehaviour
{
    [Header("UI Panels")]
    [SerializeField] GameObject inventory;
    [SerializeField] GameObject bagInventory;
    [SerializeField] GameObject map;
    [SerializeField] GameObject journal;
    [SerializeField] GameObject items;

    [Header("Map")]
    [SerializeField] GameObject defaultMap;
    [SerializeField] GameObject hoveredMap;

    [Header("Journal")]
    [SerializeField] GameObject defaultJournal;
    [SerializeField] GameObject hoveredJournal;

    [Header("Items")]
    [SerializeField] GameObject defaultItems;
    [SerializeField] GameObject hoveredItems;

    bool isInventoryOpened = false;
    int hoveredInventoryIndex = -1;
    // 0 = Map, 1 = Journal, 2 = Items
    List<bool> inventoriesOpenedList = new List<bool>();

    private void Start()
    {
        // 0 = Map, 1 = Journal, 2 = Items
        inventoriesOpenedList.Add(false);
        inventoriesOpenedList.Add(false);
        inventoriesOpenedList.Add(false);

        CloseBagInventory();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            HandleInventory();
        }

        // Check if any of detail inventory is opened
        bool isDetailInventoryOpened = hoveredInventoryIndex >= 0 ? inventoriesOpenedList[hoveredInventoryIndex] : false;

        // Handle Next or Prev Hover Inventory and Entered when Inventory Is Opened
        if (isInventoryOpened)
        {
            if (Input.GetButtonDown("Submit"))
            {
                OpenInventoryBasedOnIndex();
            }

            if (!isDetailInventoryOpened)
            {
                if (Input.GetButtonDown("Prev"))
                {
                    HoverPreviousInventory();
                }

                if (Input.GetButtonDown("Next"))
                {
                    HoverNextInventory();
                }
            }

            if (Input.GetButtonDown("Cancel"))
            {
                CloseInventory(isDetailInventoryOpened);
            }
        }
    }

    #region INVENTORY
    public void HandleInventory()
    {
        isInventoryOpened = !isInventoryOpened;
        if (isInventoryOpened)
        {
            OpenBagInventory();
        }
        else
        {
            CloseBagInventory();
        }
    }

    public void OpenBagInventory()
    {
        CommonHandler.instance.FadeInAnimation(bagInventory, inventory);

        if(hoveredInventoryIndex < 0)
        {
            HoverNextInventory();
        }
        else
        {
            HoverInventoryBasedOnIndex();
        }
    }

    public void CloseBagInventory()
    {
        if (map.activeSelf) { CommonHandler.instance.FadeOutAnimation(map, null, CommonHandler.instance.fadeOutDuration * 0.5f); }
        if (journal.activeSelf) { CommonHandler.instance.FadeOutAnimation(journal, null, CommonHandler.instance.fadeOutDuration * 0.5f); }
        if (items.activeSelf) { CommonHandler.instance.FadeOutAnimation(items, null, CommonHandler.instance.fadeOutDuration * 0.5f); }
        CommonHandler.instance.FadeOutAnimation(bagInventory, inventory);
    }

    public void HoverInventoryBasedOnIndex()
    {
        // 0 = Map, 1 = Journal, 2 = Items
        switch (hoveredInventoryIndex)
        {
            case 0:
                defaultMap.SetActive(false);
                hoveredMap.SetActive(true);
                break;
            case 1:
                defaultJournal.SetActive(false);
                hoveredJournal.SetActive(true);
                break;
            case 2:
                defaultItems.SetActive(false);
                hoveredItems.SetActive(true);
                break;
            default:
                break;
        }
    }
    public void UnhoverInventoryBasedOnIndex(int index)
    {
        // 0 = Map, 1 = Journal, 2 = Items
        switch (index)
        {
            case 0:
                hoveredMap.SetActive(false);
                defaultMap.SetActive(true);
                break;
            case 1:
                hoveredJournal.SetActive(false);
                defaultJournal.SetActive(true);
                break;
            case 2:
                hoveredItems.SetActive(false);
                defaultItems.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void HoverNextInventory()
    {
        if(hoveredInventoryIndex < 0)
        {
            hoveredInventoryIndex = 0;
            HoverInventoryBasedOnIndex();
            return;
        }

        int previousHoveredInventoryIndex = hoveredInventoryIndex;
        hoveredInventoryIndex = hoveredInventoryIndex + 1 >= inventoriesOpenedList.Count ? hoveredInventoryIndex : hoveredInventoryIndex + 1;
        if(previousHoveredInventoryIndex == hoveredInventoryIndex) { return; }
        else { UnhoverInventoryBasedOnIndex(previousHoveredInventoryIndex); }
        HoverInventoryBasedOnIndex();
    }
    public void HoverPreviousInventory()
    {
        int previousHoveredInventoryIndex = hoveredInventoryIndex;
        hoveredInventoryIndex = hoveredInventoryIndex - 1 < 0 ? 0 : hoveredInventoryIndex - 1;
        if (previousHoveredInventoryIndex == hoveredInventoryIndex) { return; }
        else { UnhoverInventoryBasedOnIndex(previousHoveredInventoryIndex); }
        HoverInventoryBasedOnIndex();
    }

    public void OpenInventoryBasedOnIndex()
    {
        // 0 = Map, 1 = Journal, 2 = Items
        if(hoveredInventoryIndex < inventoriesOpenedList.Count && hoveredInventoryIndex >= 0)
            inventoriesOpenedList[hoveredInventoryIndex] = true;
        switch (hoveredInventoryIndex)
        {
            case 0:
                OpenMap();
                break;
            case 1:
                OpenJournal();
                break;
            case 2:
                OpenItems();
                break;
            default:
                break;
        }
    }
    public void CloseInventoryBasedOnIndex()
    {
        // 0 = Map, 1 = Journal, 2 = Items
        if (hoveredInventoryIndex < inventoriesOpenedList.Count && hoveredInventoryIndex >= 0)
            inventoriesOpenedList[hoveredInventoryIndex] = false;
        switch (hoveredInventoryIndex)
        {
            case 0:
                CloseMap();
                break;
            case 1:
                CloseJournal();
                break;
            case 2:
                CloseItems();
                break;
            default:
                break;
        }
    }

    public void CloseInventory(bool isDetailOpened)
    {
        if (isDetailOpened)
        {
            CloseInventoryBasedOnIndex();
        }
        else
        {
            CloseBagInventory();
        }
    }
    #endregion

    #region MAP
    public void OpenMap()
    {
        CommonHandler.instance.FadeOutAnimation(bagInventory);
        CommonHandler.instance.FadeInAnimation(map);
    }

    public void CloseMap()
    {
        CommonHandler.instance.FadeOutAnimation(map);
        CommonHandler.instance.FadeInAnimation(bagInventory);
    }
    #endregion

    #region JOURNAL
    public void OpenJournal()
    {
        CommonHandler.instance.FadeOutAnimation(bagInventory);
        CommonHandler.instance.FadeInAnimation(journal);
    }

    public void CloseJournal()
    {
        CommonHandler.instance.FadeOutAnimation(journal);
        CommonHandler.instance.FadeInAnimation(bagInventory);
    }
    #endregion

    #region ITEMS
    public void OpenItems()
    {
        CommonHandler.instance.FadeOutAnimation(bagInventory);
        CommonHandler.instance.FadeInAnimation(items);
    }

    public void CloseItems()
    {
        CommonHandler.instance.FadeOutAnimation(items);
        CommonHandler.instance.FadeInAnimation(bagInventory);
    }
    #endregion
}
