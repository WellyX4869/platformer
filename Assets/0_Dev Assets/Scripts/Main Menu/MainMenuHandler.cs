using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuHandler : MonoBehaviour
{
    [Header("Main Menu Panels")]
    [SerializeField] GameObject startGamePanel;
    [SerializeField] GameObject creditsPanel;
    [SerializeField] GameObject quitPanel;
    
    private void Start() 
    {
        startGamePanel.SetActive(false);
        if(creditsPanel != null) { creditsPanel.SetActive(false); }
        quitPanel.SetActive(false);        
    }

    #region START GAME PANEL
    public void ShowStartGamePanel()
    {
        CommonHandler.instance.FadeInAnimation(startGamePanel);
    }

    public void HoverSaveSlot(Transform saveSlot)
    {
        if(saveSlot == null){return;}
        
        // Get Highlighted Save Slot
        GameObject highlightSaveSlot = saveSlot.GetChild(saveSlot.childCount- 1).gameObject;
        highlightSaveSlot.SetActive(true);
    }

    public void UnhoverSaveSlot(Transform saveSlot)
    {
        if (saveSlot == null) { return; }

        // Get Highlighted Save Slot
        GameObject highlightSaveSlot = saveSlot.GetChild(saveSlot.childCount - 1).gameObject;
        
        if(highlightSaveSlot.activeSelf == true){
            highlightSaveSlot.SetActive(false);
        }
    }

    public void StartGameWithSavedSlot(Transform saveSlot)
    {
        // Check if there is saved file on this save slot
        int saveSlotIndex = saveSlot.GetSiblingIndex();

        // Save game data with the save slot index

        // Go To Game
        // For now go to build index 1 for chapter 1
        SceneManager.LoadScene(1);
    }

    public void CloseStartGamePanel()
    {
        CommonHandler.instance.FadeOutAnimation(startGamePanel);
    }
    #endregion

    #region CREDITS
    #endregion

    #region QUIT PANEL
    public void ShowQuitPanel()
    {
        CommonHandler.instance.FadeInAnimation(quitPanel);
    }

    public void CancelQuitPanel()
    {
        CommonHandler.instance.FadeOutAnimation(quitPanel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    #endregion
}
