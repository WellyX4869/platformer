using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Arc1Handler : MonoBehaviour
{
    public static Arc1Handler instance;

    [SerializeField] GameObject bridge;
    [SerializeField] GameObject chains;
    [SerializeField] float bridgeRotationZ;
    [SerializeField] float bridgeRotateDuration;

    bool rotating = false;
    bool bridgeDown = false;

    private void Awake()
    {
        instance = this;
    }

    public void PlayBridgeDown()
    {
        if (bridgeDown) return;
        StartCoroutine(RotateObject(bridge, new Vector3(0, 0, bridgeRotationZ), bridgeRotateDuration));
    }

    IEnumerator RotateObject(GameObject gameObjectToMove, Vector3 eulerAngles, float duration)
    {
        if (rotating)
        {
            yield break;
        }
        rotating = true;
        bridgeDown = true;

        Vector3 newRot = gameObjectToMove.transform.eulerAngles + eulerAngles;

        Vector3 currentRot = gameObjectToMove.transform.eulerAngles;

        chains.SetActive(false);

        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            gameObjectToMove.transform.eulerAngles = Vector3.Lerp(currentRot, newRot, counter / duration);
            yield return null;
        }
        rotating = false;
        SceneManager.LoadScene(1);
    }


    #region SIGNAL RECEIVER
    public void TestSignal()
    {
        Debug.Log("Test Signal");
    }

    public void EndArc1_1()
    {
        MoreMountains.CorgiEngine.LevelClear.instance.LevelCleared();
    }
    #endregion
}
