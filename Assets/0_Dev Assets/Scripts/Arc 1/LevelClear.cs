using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine.UI;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class LevelClear : MonoBehaviour
    {
        public static LevelClear instance;

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
            }
        }

        public void LevelCleared()
        {
            CustomGameManager.Instance.ShowLevelCleared();
        }
    }
}
