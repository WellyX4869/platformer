using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainForArc1 : MonoBehaviour
{
    private void OnDisable()
    {
        if (Arc1Handler.instance != null)
            Arc1Handler.instance.PlayBridgeDown();
    }
}
